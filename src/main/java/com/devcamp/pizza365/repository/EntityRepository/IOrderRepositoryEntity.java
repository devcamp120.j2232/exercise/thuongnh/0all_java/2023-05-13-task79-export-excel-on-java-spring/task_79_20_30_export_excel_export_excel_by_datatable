package com.devcamp.pizza365.repository.EntityRepository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Order;


public interface IOrderRepositoryEntity extends JpaRepository<Order, Integer> {
     // // yêu cầu 2 Viết query cho bảng orders cho phép tìm danh sách theo order_date với LIKE
	@Query(value = "SELECT * FROM orders WHERE order_date LIKE :%order_date%", nativeQuery = true)
	List<Order> findByOrderDateLike(@Param("order_date") String orderDate);  

	// @Query(value = "SELECT * FROM orders WHERE first_name LIKE :firstName%", nativeQuery = true)
	// List<Customer> findByFirstNameLike(@Param("firstName") String firstName);

	// @Query(value = "SELECT * FROM orders WHERE city LIKE :city%", nativeQuery = true)  // yc 4 
	// List<Customer> findByCityLike(@Param("city") String city, Pageable pageable);

	// // yêu cầu 3 Viết query cho bảng orders cho phép tìm danh sách theo city, state với LIKE có phân trang.
	@Query(value = "SELECT * FROM orders WHERE order_date LIKE :%order_date%", nativeQuery = true)
	List<Order> findByOrderDateLikePage(@Param("order_date") String orderDate, Pageable pageable);

	// // yêu cầu 4 Viết query cho bảng orders cho phép tìm danh sách theo coment có phân trang và ORDER BY coment.
	@Query(value = "SELECT * FROM orders WHERE status = :status ORDER BY id ASC", nativeQuery = true)
	List<Order> findByStatusLike(@Param("status") String status, Pageable pageable);

	// // yc5  Viết query cho bảng orders cho phép UPDATE dữ liệu có comment = NULL với giá trị truyền vào từ tham số
	@Transactional   //các cập nhật cơ sở dữ liệu sẽ được hủy bỏ nếu xảy ra lỗi trong quá trình thực hiện.
	@Modifying
	@Query(value = "UPDATE orders SET comments = :comments  WHERE comments IS null", nativeQuery = true)
	int updateComment(@Param("comments") String comments);

	// // yêu cầu 6 .   Làm tương tự cho payments , orders, order_details, products, product_lines, employees, offices 
}
