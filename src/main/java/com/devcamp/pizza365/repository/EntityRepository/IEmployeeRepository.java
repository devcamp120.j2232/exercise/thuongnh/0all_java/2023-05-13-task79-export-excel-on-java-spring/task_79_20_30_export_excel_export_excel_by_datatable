package com.devcamp.pizza365.repository.EntityRepository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee, Integer> {
	boolean existsByEmail(String email);
	// yêu cầu 2 Viết query cho bảng employees cho phép tìm danh sách theo họ hoặc tên với LIKE
	@Query(value = "SELECT * FROM employees WHERE last_name LIKE :last_name%", nativeQuery = true)
	List<Employee> findByLastNameLike(@Param("last_name") String lastName);

	@Query(value = "SELECT * FROM employees WHERE first_name LIKE :firstName%", nativeQuery = true)
	List<Employee> findByFirstNameLike(@Param("firstName") String firstName);

	
	// yêu cầu 3 Viết query cho bảng employees cho phép tìm danh sách theo last_name,  với LIKE có phân trang.
	@Query(value = "SELECT * FROM employees WHERE last_name LIKE :last_name%", nativeQuery = true)
	List<Employee> findByLastNameLikePage(@Param("last_name") String lastName, Pageable pageable);

	// yêu cầu 4 Viết query cho bảng employees cho phép tìm danh sách theo first_name có phân trang và ORDER BY tên.
	@Query(value = "SELECT * FROM employees WHERE first_name = :first_name ORDER BY first_name ASC", nativeQuery = true)
	List<Employee> findByFirstNameOrderByPage(@Param("first_name") String firstName, Pageable pageable);

	// yc5  Viết query cho bảng employees cho phép UPDATE dữ liệu có first_name = NULL với giá trị truyền vào từ tham số
	@Transactional  // javax.transaction các cập nhật cơ sở dữ liệu sẽ được hủy bỏ nếu xảy ra lỗi trong quá trình thực hiện.
	@Modifying
	@Query(value = "UPDATE employees SET first_name = :first_name  WHERE first_name IS null", nativeQuery = true)
	int updateCountry(@Param("first_name") String firstName);


}
