package com.devcamp.pizza365.repository.EntityRepository;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.ProductLine;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;


public interface IProductLinesRepository extends JpaRepository<ProductLine, Long> {  
    Optional<ProductLine>  findById(long id);
       // yêu cầu 2 Viết query cho bảng product_lines cho phép tìm danh sách theo product_name với LIKE
	@Query(value = "SELECT * FROM product_lines WHERE description LIKE :%description%", nativeQuery = true)
	List<ProductLine> findByProductLineLikeDescription(@Param("description") String description);


	

	// // yêu cầu 3 Viết query cho bảng product_lines cho phép tìm danh sách theo description với LIKE có phân trang.
	@Query(value = "SELECT * FROM product_lines WHERE description LIKE :description%", nativeQuery = true)
	List<ProductLine> findByProductLineLikeDescriptionPage(@Param("description") String description, Pageable pageable);

	

	// // yc5  Viết query cho bảng product_lines cho phép UPDATE dữ liệu có description = NULL với giá trị truyền vào từ tham số
	@Transactional  //  javax.transaction các cập nhật cơ sở dữ liệu sẽ được hủy bỏ nếu xảy ra lỗi trong quá trình thực hiện.
	@Modifying
	@Query(value = "UPDATE product_lines SET description = :description  WHERE description IS null", nativeQuery = true)
	int updateCountry(@Param("description") String description);
}
