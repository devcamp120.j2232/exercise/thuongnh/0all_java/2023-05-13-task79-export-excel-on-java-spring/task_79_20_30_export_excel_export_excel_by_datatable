package com.devcamp.pizza365.repository.EntityRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Payment;

public interface IPaymentsRepository extends JpaRepository<Payment, Integer> {
   
	// // yêu cầu 6 .   Làm tương tự cho payments , orders, order_details, products, product_lines, employees, offices 

     // // yêu cầu 2 Viết query cho bảng payments cho phép tìm danh sách theo check_number  với LIKE
    @Query(value = "SELECT * FROM payments WHERE check_number LIKE :%check_number%", nativeQuery = true)
	List<Payment> findByNumberLike(@Param("check_number") String checkNumber);


     // // yêu cầu 3 Viết query cho bảng payments cho phép tìm danh sách theo payment_date với LIKE có phân trang.
     @Query(value = "SELECT * FROM payments WHERE payment_date LIKE :%payment_date%", nativeQuery = true)
	List<Payment> findByDateLike(@Param("payment_date") String pPayment);

     // bỏ qua .. có null bao giờ đâu  yc5  Viết query cho bảng payments cho phép UPDATE dữ liệu có country = NULL với giá trị truyền vào từ tham số


}
