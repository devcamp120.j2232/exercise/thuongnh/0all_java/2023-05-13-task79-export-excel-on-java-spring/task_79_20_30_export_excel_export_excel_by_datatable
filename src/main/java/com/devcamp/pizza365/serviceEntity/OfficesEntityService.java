package com.devcamp.pizza365.serviceEntity;
import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.EntityRepository.IOfficeRepository;

@Service
public class OfficesEntityService {

    @Autowired
	IOfficeRepository gOfficeRepository;

	public ResponseEntity<List<Office>> getAllOffice() {
		try {
			List<Office> vOffices = new ArrayList<Office>();
			gOfficeRepository.findAll().forEach(vOffices::add);
			return new ResponseEntity<>(vOffices, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<Object> getOfficeById( Integer id) {
		Optional<Office> vOfficeData = gOfficeRepository.findById(id);
		if (vOfficeData.isPresent()) {
			try {
				Office vOffice = vOfficeData.get();
				return new ResponseEntity<>(vOffice, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Office vOfficeNull = new Office();
			return new ResponseEntity<>(vOfficeNull, HttpStatus.NOT_FOUND);
		}
	}

	public ResponseEntity<Object> createOffice(@Valid @RequestBody Office paramOffice) {
		try {
			Office vOffice = new Office();
			vOffice.setCity(paramOffice.getCity());
			vOffice.setPhone(paramOffice.getPhone());
			vOffice.setAddressLine(paramOffice.getAddressLine());
			vOffice.setState(paramOffice.getState());
			vOffice.setCountry(paramOffice.getCountry());
			vOffice.setTerritory(paramOffice.getTerritory());
			Office vOfficeSave = gOfficeRepository.save(vOffice);
			return new ResponseEntity<>(vOfficeSave, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Office: " + e.getCause().getCause().getMessage());
		}
	}

	public ResponseEntity<Object> updateOffice( Integer id, @Valid @RequestBody Office paramOffice) {
		Optional<Office> vOfficeData = gOfficeRepository.findById(id);
		if (vOfficeData.isPresent()) {
			try {
				Office vOffice = vOfficeData.get();
				vOffice.setCity(paramOffice.getCity());
				vOffice.setPhone(paramOffice.getPhone());
				vOffice.setAddressLine(paramOffice.getAddressLine());
				vOffice.setState(paramOffice.getState());
				vOffice.setCountry(paramOffice.getCountry());
				vOffice.setTerritory(paramOffice.getTerritory());
				Office vOfficeSave = gOfficeRepository.save(vOffice);
				return new ResponseEntity<>(vOfficeSave, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Office: " + e.getCause().getCause().getMessage());
			}
		} else {
			Office vOfficeNull = new Office();
			return new ResponseEntity<>(vOfficeNull, HttpStatus.NOT_FOUND);
		}
	}

	public ResponseEntity<Object> deleteOfficeById( Integer id) {
		Optional<Office> vOfficeData = gOfficeRepository.findById(id);
		if (vOfficeData.isPresent()) {
			try {
				gOfficeRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Office vOfficeNull = new Office();
			return new ResponseEntity<>(vOfficeNull, HttpStatus.NOT_FOUND);
		}
	}
    
}
