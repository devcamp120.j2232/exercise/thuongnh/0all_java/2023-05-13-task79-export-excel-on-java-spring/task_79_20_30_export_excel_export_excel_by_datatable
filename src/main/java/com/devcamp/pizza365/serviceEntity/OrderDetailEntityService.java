package com.devcamp.pizza365.serviceEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.repository.EntityRepository.IOrderDetailRepository;

@Service
public class OrderDetailEntityService {
    @Autowired
	IOrderDetailRepository pIOrderDetailRepository;  // thuộc tính 
   

    // Viết code CRUD Rest API bảng customers (yêu cầu dùng service class).
     // 1 all
     public ResponseEntity<Object> getAllService() {
        try {
            System.out.println("thu thanh conng service");
            List<OrderDetail> newObject = new ArrayList<OrderDetail>();
            pIOrderDetailRepository.findAll().forEach(newObject::add);
            return new ResponseEntity<>(newObject, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     // 2 thông tin album theo id
     public ResponseEntity<Object> getByIdService( int id) {
         try {
            System.out.println("thu thanh conng service");
             Optional<OrderDetail> newObject = pIOrderDetailRepository.findById(id);
             // Optional để giải quyết các vấn đề về truy cập vào các đối tượng có thể null.
             // Nó đóng vai trò là một bao bọc an toàn cho các giá trị có thể không tồn tại,
             // giúp tránh những lỗi NullPointerException khi truy cập vào đối tượng null.
             if (newObject.isPresent()) {
                 return new ResponseEntity<>(newObject.get(), HttpStatus.OK);
             } else {
                 return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);// không timg thấy
             }
         } catch (Exception e) {
             // TODO: handle exception
             System.out.println(e);
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // lỗi máy máy chủ
         }
     }
      //>>>> 3 tạo mới album
     public ResponseEntity<Object> createService( OrderDetail FormClient) {
        try {
           
             OrderDetail retsulSaver  = pIOrderDetailRepository.save(FormClient);
            return new ResponseEntity<Object>(retsulSaver, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("phát hiện lỗi như sau:  " + e.getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("phát hiện lỗi như sau: " + e.getCause().getCause().getMessage());
        }
    }
    // 4 update
    public ResponseEntity<Object> updateService(@PathVariable(name = "id") Integer paramId,
            OrderDetail FormClient) {
        // tìm album theo id trên data base
        Optional<OrderDetail> _customerData = pIOrderDetailRepository.findById(paramId);
        // kiểm tra có null hay k ,, true là khác null
        if (_customerData.isPresent()) {
            try {
               
                //_customer.setImages(FormClient.getImages());  // nếu bạn thích tính năng này thì hủy comment . riêng tôi k thích
                pIOrderDetailRepository.save(FormClient);
                return ResponseEntity.ok(pIOrderDetailRepository.save(FormClient));
            } catch (Exception e) {
                // TODO: handle exception
                System.out.println("phát hiện lỗi như sau:  " + e.getMessage());
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

      // 5 delete album //thì xóa luôn all orders thuộc album đó
    public ResponseEntity<Object> deleteService(Integer id) {
        Optional<OrderDetail> newObject = pIOrderDetailRepository.findById(id);
        // nếu khác null
        if (newObject.isPresent()) {
            try {
                pIOrderDetailRepository.deleteById(id); // delete album trên data base
                return new ResponseEntity<Object>("đã xóa album có id là " + id,HttpStatus.OK);
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity().body("can not execute operation of this Entity" +e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }
        } else {
            return new ResponseEntity<Object>("không tìm thấy album",HttpStatus.NOT_FOUND);
        }
    }
}
