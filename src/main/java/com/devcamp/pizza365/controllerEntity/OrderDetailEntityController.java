package com.devcamp.pizza365.controllerEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.serviceEntity.OrderDetailEntityService;

@RestController
@CrossOrigin
@RequestMapping("/order/detail/entity")
public class OrderDetailEntityController {

    @Autowired
	OrderDetailEntityService pOrderDetailEntityService;
	// Viết code CRUD Rest API bảng customers (yêu cầu dùng service class).
	  // 1 all
	  @GetMapping("/all")
	  public ResponseEntity<Object> getAllAlbum() {
		  return pOrderDetailEntityService.getAllService();
	  }
  
	  // 2 thông tin user theo id
	  @GetMapping("/detail/{paramId}")
	  public ResponseEntity<Object> getAlbumById(@PathVariable Integer paramId) {
		  return pOrderDetailEntityService.getByIdService(paramId);
	  }
  
	//   {
    //     "id": 1,
    //     "quantityOrder": 30,
    //     "priceEach": 136.00
    // },
	  @PostMapping("/create")
	  public ResponseEntity<Object> createAlbum(@RequestBody OrderDetail FormClient) {
		  return pOrderDetailEntityService.createService(FormClient);
	  }
  
	  // 4 update
	  @PutMapping("/update/{id}")
	  public ResponseEntity<Object> updateUser(@PathVariable(name = "id") int paramId,
			  @RequestBody OrderDetail FormClient) {
				  return pOrderDetailEntityService.updateService(paramId, FormClient);
	  }
  
	  // 5 delete user //thì xóa luôn all orders thuộc user đó
	  @DeleteMapping("/delete/{id}")
	  public ResponseEntity<Object> deleteUser(@PathVariable int id) {
		  return pOrderDetailEntityService.deleteService(id);
	  }
    
}
