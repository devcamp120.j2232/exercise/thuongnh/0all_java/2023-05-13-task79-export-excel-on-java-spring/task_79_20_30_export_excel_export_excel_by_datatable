package com.devcamp.pizza365.controllerEntity;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.EntityRepository.ICustomerRepository;
import com.devcamp.pizza365.service.ExcelExporter;
import com.devcamp.pizza365.serviceEntity.CustomersService;

@RestController
@CrossOrigin
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	ICustomerRepository customerRep;
	@Autowired
	CustomersService pCustomersService;
	// Viết code CRUD Rest API bảng customers (yêu cầu dùng service class).
	  // 1 all
	  @GetMapping("/all")
	  public ResponseEntity<Object> getAllAlbum() {
		  return pCustomersService.getAllService();
	  }
  
	  // 2 thông tin user theo id
	  @GetMapping("/detail/{paramId}")
	  public ResponseEntity<Object> getAlbumById(@PathVariable Integer paramId) {
		  return pCustomersService.getByIdService(paramId);
	  }
  
	//   {
	// 
	// 	"lastName": "Berglund",
	// 	"firstName": "Christina",
	// 	"phoneNumber": "0921-12 3555",
	// 	"address": "Berguvsvägen 8",
	// 	"city": "Luleå",
	// 	"state": "null",
	// 	"postalCode": "S-958 22",
	// 	"country": "Sweden",
	// 	"salesRepEmployeeNumber": 1504,
	// 	"creditLimit": 53100
	// }
	  @PostMapping("/create")
	  public ResponseEntity<Object> createAlbum(@RequestBody Customer albumFormClient) {
		  return pCustomersService.createService(albumFormClient);
	  }
  
	  // 4 update
	  @PutMapping("/update/{id}")
	  public ResponseEntity<Object> updateUser(@PathVariable(name = "id") int paramId,
			  @RequestBody Customer albumFormClient) {
				  return pCustomersService.updateService(paramId, albumFormClient);
	  }
  
	  // 5 delete user //thì xóa luôn all orders thuộc user đó
	  @DeleteMapping("/delete/{id}")
	  public ResponseEntity<Object> deleteUser(@PathVariable int id) {
		  return pCustomersService.deleteService(id);
	  }


	// tìm kiếm bảng ghi của table customers theo last name 
	@GetMapping("/last-name/{lastName}")
	public ResponseEntity<List<Customer>> getCustomersByLastNameLike(@PathVariable String lastName) {
		return pCustomersService.getCustomersByLastNameLikeService(lastName);
	}

	@GetMapping("/first-name/{firstName}")
	public ResponseEntity<List<Customer>> getCustomersByFirstNameLike(@PathVariable String firstName) {
		return pCustomersService.getCustomersByFirstNameLike(firstName);
	}

	@GetMapping("/city/{city}")
	public ResponseEntity<List<Customer>> getCustomersByCityLike(@PathVariable String city,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		return pCustomersService.getCustomersByCityLike(city, page);
	}

	@GetMapping("/state/{state}")
	public ResponseEntity<List<Customer>> getCustomersByStateLike(@PathVariable String state,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
	return pCustomersService.getCustomersByStateLike(state, page);
	}

	@GetMapping("/country/{country}")
	public ResponseEntity<List<Customer>> getCustomersByCountryLike(@PathVariable String country,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		return pCustomersService.getCustomersByCountryLike(country, page);
	}

	@PutMapping("/update/{country}")
	public ResponseEntity<Object> updateCountry(@PathVariable String country) {
		return pCustomersService.updateCountry(country);
	}

	@GetMapping("/group/by/country/quantity/customer")
	public ResponseEntity<List<Object>>  groupCountryQuantityCustomer() {
		return pCustomersService.groupCountryQuantityCustomer();
	}


	// http://localhost:8080/customers/export/customers/excel
	// HttpServletResponse response: đối tượng dùng để gửi HTTP response về client.
	@GetMapping("/export/customers/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");  //đầu ra của file được xuất ra là một stream nhị phân (binary stream) được truyền tải đến trình duyệt.
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";   // trình duyệt sẽ hiển thị hộp thoại tải xuống (download dialog) cho người dùng để lưu file 
		String headerValue = "attachment; filename=customers_" + currentDateTime + ".xlsx";  // Nếu giá trị của Content-Disposition là "attachment" thì 
		// cho phép máy khách (client) biết nên xử lý response như thế nào, ví dụ như hiển thị trực tiếp nội dung trên trình duyệt, tải xuống hoặc hiển thị một thông báo trên màn hình của người dùng.
		response.setHeader(headerKey, headerValue);

		List<Customer> customer = new ArrayList<Customer>();
		// customerRep chính là repository
		customerRep.findAll().forEach(customer::add);  

		ExcelExporter excelExporter = new ExcelExporter(customer);  //  khởi tạo server export danh sách Customer 

		excelExporter.export(response);  // đối tượng tự tạo để xử lý bảng excel 
	}



}
