package com.devcamp.pizza365.controllerEntity;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.serviceEntity.EmployeeEntityService;

@RestController
@CrossOrigin
@RequestMapping("/employee")
public class EmployeeController {
	@Autowired
	EmployeeEntityService pEmployeeEntityService;

	@GetMapping("/all")
	public ResponseEntity<List<Employee>> getAllEmployee() {
		return pEmployeeEntityService.getAllEmployee();
	}

	@GetMapping("/details/{id}")  // 1056
	public ResponseEntity<Object> getEmployeeById(@PathVariable Integer id) {
		return pEmployeeEntityService.getEmployeeById(id);
	}



	@PostMapping("/create")
	public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee paramEmployee) {
		return pEmployeeEntityService.createEmployee(paramEmployee);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateEmployee(@PathVariable Integer id, @Valid @RequestBody Employee paramEmployee) {
		return pEmployeeEntityService.updateEmployee(id, paramEmployee);
	}

	@DeleteMapping("/delete/{id}")
	private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
		return pEmployeeEntityService.deleteProductById(id);
	}
	@GetMapping("/exists/{email}")
	public ResponseEntity<Object> isExistsEmail(@PathVariable String email) {
		return pEmployeeEntityService.isExistsEmail(email);
	}


}
