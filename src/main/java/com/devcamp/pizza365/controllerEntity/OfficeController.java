package com.devcamp.pizza365.controllerEntity;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.serviceEntity.OfficesEntityService;



@RestController
@CrossOrigin
@RequestMapping("/office")
public class OfficeController {
	@Autowired
	OfficesEntityService pOfficesEntityService;

	@GetMapping("/all")
	public ResponseEntity<List<Office>> getAllOffice() {
		return pOfficesEntityService.getAllOffice();
	}
	// 6 
	@GetMapping("/details/{id}")
	public ResponseEntity<Object> getOfficeById(@PathVariable Integer id) {
		return pOfficesEntityService.getOfficeById(id);
	}

	@PostMapping("/create")
	public ResponseEntity<Object> createOffice(@Valid @RequestBody Office paramOffice) {
		return pOfficesEntityService.createOffice(paramOffice);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateOffice(@PathVariable Integer id, @Valid @RequestBody Office paramOffice) {
		return pOfficesEntityService.updateOffice(id, paramOffice);
	}

	@DeleteMapping("/delete/{id}")
	private ResponseEntity<Object> deleteOfficeById(@PathVariable Integer id) {
		return pOfficesEntityService.deleteOfficeById(id);
	}
}
